# DESCRIPTION

Provide a role so you can easily implement a JWT provider

# SYNOPSIS

This is for example on how to implement Azure for JWT tokens

    class Auth::JWT::Azure :does(Auth::JWT::Provider);
    use LWP::UserAgent;

    sub BUILDARGS {
        my $self = shift;
        my %args = @_;
        $args{wellknown} = [ ... ];
        $args{audience} = [ ... ];
        $args{scope} = [ ... ];
    }

    method ua() {
      return $ua if $ua;
      $ua = LWP::UserAgent->new(
        timeout           => 10,
        agent             => join('/', __PACKAGE__, $VERSION),
        allowed_protocols => [qw(https)],
      );
      return $ua;
    }


    my $azure = class Auth::JWT::Azure->new(..)
    my $data = $azure->decode_token($token);
    $azure->validate_audience($data);
    $azure->validate_scope($data);

# ATTRIBUTES

# METHODS

## get\_kid\_keys

Get all the kid keys from the well known locations, returns a structure that
can be directly used in [decode\_token](https://metacpan.org/pod/decode_token), `{ keys => \@keys }`.

## get\_kid\_key($uri)

Get the key from a given location.

## decode\_token($token, $warning)

Decodes the token, without the warning boolean set to true this is a fatal
error, otherwise returns undef when a token cannot be decoded.

## validate\_audience($data)

Validate the audience of the token

## validate\_scope($data)

Validate the scope of the token

# CACHING

It is advisable to cache the keys in your application. Do do that override
either the [get\_kid\_key](https://metacpan.org/pod/get_kid_key) or [get\_kid\_keys](https://metacpan.org/pod/get_kid_keys) method.
