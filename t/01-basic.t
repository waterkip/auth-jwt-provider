package Test::JWT;
use v5.26;
use Object::Pad;

# ABSTRACT: Test::JWT needs a propper abstract

class Test::JWT : does(Auth::JWT::Provider);
use LWP::UserAgent;
use Data::Dumper;

sub BUILDARGS {
    my $self = shift;
    my %args = @_;
    $args{wellknown} = [qw(https://example.com)];
    $args{audience} = [qw(audienceHere)];
    $args{scope} = [qw(some scope)];
    return %args;
}

method ua() {
  return LWP::UserAgent->new();
}


1;

package main;
use strict;
use warnings;
use Test::More 0.96;
use Test::Deep;
use Test::Exception;

use Sub::Override;
use Test::Mock::One;

my $override = Sub::Override->new(
  'LWP::UserAgent::get' => sub {
    my $self = shift;
    my $uri  = shift;
    if ($uri eq 'https://example.com') {
      return Test::Mock::One->new(
        decoded_content => '{"jwks_uri":"https://jwt.example.com"}',
        is_success      => 1
      );
    }
    if ($uri eq 'https://jwt.example.com') {
      return Test::Mock::One->new(
        decoded_content => '{ "keys": [ { "kid": "xxx" } ] }',
        is_success      => 1
      );
    }
    return;
  }
);

my $jwt = Test::JWT->new(application_id => '1234');

my $key = $jwt->get_kid_key('https://example.com');
cmp_deeply($key, { keys => [{ kid => 'xxx' }] }, "Got the correct kid key");

my $keys = $jwt->get_kid_keys();
cmp_deeply($keys, { keys => [[{ kid => 'xxx' }]] },
  "Got the correct kid keys");

throws_ok(
  sub {
    $jwt->decode_token('foo');
  },
  qr/Unable to validate token: JWT: invalid token/,
  "Fatal error"
);

my @warnings;
local $SIG{__WARN__} = sub { push(@warnings, shift) };
ok(!$jwt->decode_token('foo', 1), "Token could not be validated via Crypt::JWT");

is(@warnings, 1, ".. as said by Crypt::JWT");
like(
  $warnings[0],
  qr/Unable to validate token: JWT: invalid token format.*/,
  ".. w/ the correct message"
);

$override->override('Auth::JWT::Provider::decode_jwt' => sub { return "Token" }
);

my $data = $jwt->decode_token('foo');
is($data, "Token", "Calling decode_jwt went succesfull");


ok($jwt->validate_audience({ aud => 'audienceHere'}), "We have validated the audience");
ok(!$jwt->validate_audience({}), ".. and fails without it");

ok($jwt->validate_scope({scp => 'some'}), "Some scope allowed");
ok(!$jwt->validate_scope({scp => 'nope'}), ".. and some not");

done_testing;
