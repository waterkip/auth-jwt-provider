package Auth::JWT::Provider;
use v5.26;
use Object::Pad;

# ABSTRACT: Implement JWT provider logic for getting keys and decoding tokens

role Auth::JWT::Provider;

use Crypt::JWT qw(decode_jwt);
use feature 'try';
no warnings 'experimental::try';
use JSON::XS qw(decode_json);
use List::Util qw(any);

method ua;

field @keys;
field @audience;
field @wellknowns;
field @scope;
field $application_id :param;

use Carp qw(croak);

ADJUSTPARAMS {
    my $args = shift;

    if (ref $args->{wellknown} eq 'ARRAY') {
        @wellknowns = @{delete $args->{wellknown}};
    }
    if (ref $args->{audience} eq 'ARRAY') {
        @audience = @{delete $args->{audience}};
    }
    if (ref $args->{scope} eq 'ARRAY') {
        @scope = @{delete $args->{scope}};
    }

    die "No well knowns URI's defined!", $/ unless @wellknowns;
    die "No audience defined!", $/ unless @audience;
}

method get_kid_keys() {

  return @keys if @keys;


  foreach (@wellknowns) {
    push(@keys, $self->get_kid_key($_)->{keys});

  }
  return { keys => \@keys };
}

method validate_audience($data) {
    my $audience = $data->{aud} // '';
    return any { $audience eq $_ } @audience;
}

method validate_scope($data) {
    return 1 unless @scope;
    my $scope = $data->{scp} // $data->{scope} // '';
    return any { $scope eq $_ } @scope;
}

method get_kid_key($uri) {
  my $res = $self->ua->get($self->_get_wellknown($uri));
  return decode_json($res->decoded_content) if $res->is_success;
  die "Unable to kid keys from well known $uri\n";
}

method _get_wellknown($uri) {
  my $res = $self->ua->get($uri);
  die "Unable to get well known from URI $uri\n" unless $res->is_success;
  return decode_json($res->decoded_content)->{jwks_uri};
}

method decode_token($token, $warn = 0) {

    my $keys = $self->get_kid_keys();
    my $data;

    try {
        $data = decode_jwt(token => $token, kid_keys => $keys);
    }
    catch($e) {
        if ($warn) {
            warn "Unable to validate token: $e \n";
            return;
        }
        die "Unable to validate token: $e \n";
    }
    return $data if $data;
    return;
}

1;

__END__

=head1 DESCRIPTION

Provide a role so you can easily implement a JWT provider

=head1 SYNOPSIS

This is for example on how to implement Azure for JWT tokens

    class Auth::JWT::Azure :does(Auth::JWT::Provider);
    use LWP::UserAgent;

    sub BUILDARGS {
        my $self = shift;
        my %args = @_;
        $args{wellknown} = [ ... ];
        $args{audience} = [ ... ];
        $args{scope} = [ ... ];
    }

    method ua() {
      return $ua if $ua;
      $ua = LWP::UserAgent->new(
        timeout           => 10,
        agent             => join('/', __PACKAGE__, $VERSION),
        allowed_protocols => [qw(https)],
      );
      return $ua;
    }


    my $azure = class Auth::JWT::Azure->new(..)
    my $data = $azure->decode_token($token);
    $azure->validate_audience($data);
    $azure->validate_scope($data);

=head1 ATTRIBUTES

=head1 METHODS

=head2 get_kid_keys

Get all the kid keys from the well known locations, returns a structure that
can be directly used in L<decode_token>, C<< { keys => \@keys } >>.

=head2 get_kid_key($uri)

Get the key from a given location.

=head2 decode_token($token, $warning)

Decodes the token, without the warning boolean set to true this is a fatal
error, otherwise returns undef when a token cannot be decoded.

=head2 validate_audience($data)

Validate the audience of the token

=head2 validate_scope($data)

Validate the scope of the token

=head1 CACHING

It is advisable to cache the keys in your application. Do do that override
either the L<get_kid_key> or L<get_kid_keys> method.
